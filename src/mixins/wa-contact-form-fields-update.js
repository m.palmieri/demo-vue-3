import { mapActions } from 'vuex';

export default {
  computed: {
    disableSubmit() {
      if (this.$v && (this.$v.$anyError || this.$v.$invalid)) {
        return true;
      }

      return false;
    }
  },
  methods: {
    ...mapActions('contact', [
      'setFormData'
    ]),
    onFieldChange(k, v) {
      this[k] = v ? v : null;

      let obj = {};
      obj[k] = v ? v : null;

      this.setFormData(obj);

      if (this.$v[k]) {
        this.$v[k].$touch();
      }
    },
    async onPhoneChange(k, v) {
      let prefix = this.phone_prefix;
      let number = this.phone_number;
      if (k === 'phone_prefix') {
        prefix = v ? v : null;
      } else if (k === 'phone_number') {
        number = v ? v : null;
      }
      this.setFormData({phone_prefix: prefix});
      this.setFormData({phone_number: number});
      this.$v.phone_prefix.$touch();
      this.$v.phone_number.$touch();
    },
    onSubmitBtnClick() {
      const me = this;
      me.$v.$touch();
      if (!me.$v.$invalid) {
        me.$refs.$form.querySelector('[type=submit]').setAttribute('disabled', true);
        if (me.$refs.$form.querySelector('[type=submit]')) {
          me.$refs.$form.querySelector('[type=submit]').removeAttribute('disabled');
        }
        this.$emit('submit');
      } else {
        console.log('invalid data');
      }
    }
  }
};
