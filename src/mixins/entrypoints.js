import Vue from 'vue';
import store from '@store';

import VueBootstrapper from 'vue-bootstrapper';
import VueViewportManager from 'vue-viewport-manager';
import VueDjangoCMSPatch from '@helpers/vue_djangocms_patch';
import '@helpers/load-components';
import Swiper, { Navigation, Pagination, Scrollbar, EffectFade, Autoplay } from 'swiper';
import VModal from 'vue-js-modal';
import VueExpand from '@plugins/vue-expand';
import Vuelidate from 'vuelidate';
import VTooltip from 'v-tooltip';
import checkView from 'vue-check-view';

import A11Support from '@mixins/a11-support';
import CmsSupport from '@mixins/cms-support';
import StringSupport from '@mixins/string-support';
import HeaderSupport from '@mixins/header-support';

import { debug } from '@helpers/utils';

/* eslint-disable */
// Adding plugins
Vue.mixin(A11Support);
Vue.mixin(CmsSupport);
Vue.mixin(StringSupport);
Vue.use(VueBootstrapper, { store });
Vue.use(VueViewportManager, {
  store,
  breakpoints: { xs: 0, sm: 768, md: 1024, lg: 1440, xl: 9999 }
});
Vue.use(VModal);
Vue.use(VueExpand);
Vue.use(Vuelidate);
Swiper.use([Navigation, Pagination, Scrollbar, EffectFade, Autoplay]);
Vue.use(VTooltip);
Vue.use(checkView);

// Ignore Django CMS template tags
Vue.config.ignoredElements = [
  'django-cms-template',
  'django-cms-plugin',
  'cms-template',
  'cms-plugin'
];


// Print some debug messages
if (debug) {
  console.log('%c[VUE_APP_ENV DEVELOPMENT]', 'padding:4px;background-color:orange;color:white;');
  console.log('%c[ADDED VUE TO WINDOW]', 'padding:4px;background-color:orange;color:white;');
  console.log('%c[VUE DEVTOOLS ENABLED]', 'padding:4px;background-color:orange;color:white;');
  window.Vue = Vue;
  Vue.config.devtools = true;
}

let DjangoPatch = null;
/* eslint-enable */

//wa-main-content
const app = document.getElementById('app-root');
app.removeAttribute('data-v-cloak');
app.setAttribute('v-cloak', '');

const Entrypoints = Vue.extend({
  mixins: [HeaderSupport],
  store: store,
  watch: {
    $currentScrollPosition: {
      handler() {
        document.documentElement.style.setProperty('--scroll-y', `${window.scrollY}px`);
      },
      immediate: true
    }
  },
  data(){
    return {
      count:3
    }
  },
  //router,
  created() {
    /* eslint-disable */
    DjangoPatch = new VueDjangoCMSPatch(this, '#app-root');
    /* eslint-enable */

    if (typeof(Event) === 'function') {
      // modern browsers
      window.dispatchEvent(new Event('vueAppReady'));
    } else {
      //This will be executed on old browsers and especially IE
      var resizeEvent = window.document.createEvent('UIEvents');
      resizeEvent.initUIEvent('vueAppReady', true, false, window, 0);
      window.dispatchEvent(resizeEvent);
    }
  },
  mounted() {
    this.mirageKiller();
  },
  methods: {
    mirageKiller() {
      (() => {
        let doCheck = () => {
          // feature working suppressing debug
          // if (debug) {
          //   console.log('%c[KILLING MIRAGE]', 'padding:4px;background-color:red;color:white;');
          // }
          var images = Array.prototype.slice.call(document.getElementsByTagName('img')),
              cfSrc,
              cfStyle;

          for (var i = 0, image = images[i]; i < images.length; image = images[++i]) {

            if ('getAttribute' in image) {

              cfSrc = image.getAttribute('data-cfsrc');
              cfStyle = image.getAttribute('data-cfstyle');
            }

            cfSrc = cfSrc || image.attributes['data-cfsrc'];
            cfStyle = cfStyle || image.attributes['data-cfstyle'];

            if (cfSrc){
              image.src = cfSrc;
            }

            if (cfStyle){
              image.style.cssText = cfStyle;
            }
          }
          this.count++;
          if(this.count!==10){
            setTimeout(doCheck, Math.exp(this.count));
          }
        }
        doCheck();
      })()
    }
  }

});

export default Entrypoints;