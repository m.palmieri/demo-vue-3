# Plugins

This folder shall contain all project-specific (or still in development) Vue mixins.

Each mixin must be written in a single file, whose name should end with `_mixin.js`. The file must `export default` the mixin itself.

## Examples

This folder can contain:

- `my_mixin.js` defines the mixin and imports its dependencies
