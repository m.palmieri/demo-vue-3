/* Mutations, actions and getters */
import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default {
  namespaced: true,
  state: {
    inverted_header: false,
    inverted_footer: false,
    date_format: '',
    edit_profile_label: '',
    gmap_key: '',
    home_page_url: '',
    language_chooser_label: '',
    link_cv: '',
    load_more_label: '',
    logout_label: '',
    new_label: '',
    recaptcha_key: '',
    languages: [],
    apis: [],
    recipe_labels:{
      all_recipes_link:"",
      back_link_label:"",
      buy_now_label:"",
      discover_more_label:"",
      how_to_label:"",
      ingredients_label:"",
      kcal_label:"",
      made_with_label:"",
      people_label:"",
      related_description_label:"",
      related_title_label:"",
      see_more_link_label:""
    },
    hideRecaptcha:false
  },
  actions,
  mutations,
  getters
};
