# Store

This directory should contain all `Vuex` store files.

## Structure

Both modules and store root must contain at least 5 files:

- `index.js` defines the state and imports *getters*, *actions* and *mutations*. The `index.js` for the **root of the store** must `export default new Vuex.store({...})`, meanwhile module-related indices must simply `export` the module itself
- `actions.js` exports the *actions* that can be dispatched in the module. Every action **must** return a `Promise` and **can** commit mutations
- `mutations.js` exports all the *mutations* available to the module. No custom name must be used for mutation functions, but all of them shall be declared as `[types.MUTATION_NAME] (state, {...}) { /* code */}`, where `types` is imported from `mutation_types.js`
- `getters.js` exports all the getters for the module, all as arrow functions, as per the [docs](https://vuex.vuejs.org/guide/getters.html)
- `mutation_types.js` exports all mutation types as strings, e.g. `export const MUTATION_NAME = 'MUTATION_NAME'`

## Examples

This folder contains:

- `store/index.js` contains the complete store definition, with `rootState` and module injection
- `store/my_module/*` contains all the files needed to completely define a `Vuex` module, i.e. `index.js`, `actions.js`, `mutations.js` and `getters.js`
- `store/my_module/mutation_types.js` contains all module-related mutation definitions, exported as strings
