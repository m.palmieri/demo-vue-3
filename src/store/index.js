/* Import dependencies */
/**
 * Ho sostituito import Vuex from 'vuex' con il nuovo metodo definito in vue3 - creteStore
 */
import { createStore } from "vuex";

/* Mutations, actions and getters */
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";

/* Modules */
// Import modules here
import config from "./config";
//import contact from "./contact";

import icons from "./icons";
/*
 * Ho sostituito l'export con la costante store in cui ho definito la configurazione dello store di cui poi ho fatto l'export.
 */
const store = createStore({
  state() {
    return {
      appReady: false,
      loading: [true],
      unlockedView: null,
    };
  },
  actions,
  mutations,
  getters,
  modules: {
    // Register modules here
    config,
    icons,
  },
});
export default store;
