import * as types from '../mutation_types';

export default {
  [types.SET_CONFIG_PROPERTY](state, payload) {
    state[payload.key] = payload.value;
  }
};
