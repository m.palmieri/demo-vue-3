import * as types from '../mutation_types';

export default {
  setConfig({commit}, payload) {
    return new Promise((resolve, reject) => {
      try {
        Object.keys(payload).forEach(k => {
          commit(types.SET_CONFIG_PROPERTY, {key: k, value: payload[k]});
        });
        resolve();
      } catch (e) {
        console.log(e);
        reject(e);
      }
    });
  }
};
