export default {
  languages: [
    {
      site_code: 'IT-it',
      language_code: 'IT-it',
      country_label: 'Italy',
    },
    {
      site_code: 'EN-en',
      language_code: 'EN-en',
      country_label: 'Italy',
    },
  ],
};
