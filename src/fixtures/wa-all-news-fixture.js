export default {
  news: [
    {
      has_tag: true,
      has_border: true,
      label: "High-protein",
      label_color: "white",
      label_background_color: "secondary-lilac",
      card_title: "1",
      card_small_title: "14 October 2020",
      card_href: "#",
      card_img_src: "https://picsum.photos/200/300",
      card_img_alt: "immagine di test",
    },
    {
      has_tag: true,
      has_border: true,
      label: "High-protein",
      label_color: "white",
      label_background_color: "secondary-lilac",
      card_title: "Test Generic Card",
      card_small_title: "14 October 2020",
      card_href: "#",
      card_img_src: "https://picsum.photos/200/300",
      card_img_alt: "immagine di test"
    },
    {
      has_tag: true,
      has_border: true,
      label: "High-protein",
      label_color: "white",
      label_background_color: "secondary-lilac",
      card_title: "Test Generic Card",
      card_small_title: "14 October 2020",
      card_href: "#",
      card_img_src: "https://picsum.photos/200/300",
      card_img_alt: "immagine di test"
    },
    {
      has_tag: true,
      has_border: true,
      label: "High-protein",
      label_color: "white",
      label_background_color: "secondary-lilac",
      card_title: "Test Generic Card",
      card_small_title: "14 October 2020",
      card_href: "#",
      card_img_src: "https://picsum.photos/200/300",
      card_img_alt: "immagine di test"
    }
  ]
};
