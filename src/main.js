import { createApp } from "vue";
import store from "./store/index.js";


import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./scss/common.scss";

import App from "./App.vue";
import CommonIcons from "./components/common-icons.vue";
import WaFooter from "./components/wa-footer.vue";
import WaFooterCol from "./components/wa-footer-col.vue";
import WaFooterMenuItem from "./components/wa-footer-menu-item.vue";
import WaSocialShare from "./components/wa-social-share.vue";
import WaSocialShareItem from "./components/wa-social-share-item.vue";
import WaYoutubeMediaPlayer from "./components/wa-youtube-media-player.vue";
import Emit from "./components/emit.vue";


const app = createApp(App);

app.component("common-icons", CommonIcons);
app.component("wa-footer", WaFooter);
app.component("wa-footer-col", WaFooterCol);
app.component("wa-footer-menu-item", WaFooterMenuItem);
app.component("wa-social-share", WaSocialShare);
app.component("wa-social-share-item", WaSocialShareItem);
app.component("wa-youtube-media-player", WaYoutubeMediaPlayer);
app.component("emit", Emit);

app.use(store);

app.mount("#app");
