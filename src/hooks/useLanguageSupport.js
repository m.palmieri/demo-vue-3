//import moment from 'moment';
import { computed } from "vue";
import languageFixture from "../fixtures/language-fixture.js";

export default function useLanguageSupport() {
  const langs = computed(() => {
    return languageFixture.languages;
  });

  const currentCountry = computed(() => {
    const country = langs.value.length
      ? langs.value.find((l) => l.site_code === l.language_code)
      : null;

    if (country && "country_label" in country) {
      return country.country_label;
    } else {
      return "";
    }
  });
  const currentLang = computed(() => {
    if (langs.value.length) {
      const country = langs.value.length
        ? langs.value.find((l) => l.site_code === l.language_code)
        : null;

      if (country && "language_code" in country) {
        const l = country.language_code;
        if (l.indexOf("-") !== -1) {
          return l.split("-")[0].toUpperCase();
        } else if (l.toLowerCase() === "global") {
          return "EN";
        } else {
          return l;
        }
      } else {
        return "EN";
      }
    } else {
      return "EN";
    }
  });

  return {
    currentCountry,
    currentLang,
  };
}
