import { ref, onMounted } from "vue";

export default function useViewportManager() {
  const step = ref("");
  function reportWindowSize() {
    let breakpoints = { xs: 0, sm: 768, md: 1024, lg: 1440, xl: 9999 };
    let currentStep = window.innerWidth;
    if (currentStep < breakpoints.sm) {
      step.value = "xs";
    } else if (currentStep < breakpoints.md) {
      step.value = "sm";
    } else if (currentStep < breakpoints.lg) {
      step.value = "md";
    } else if (currentStep < breakpoints.xl) {
      step.value = "lg";
    } else {
      step.value = "xl";
    }
    return step;
  }

  onMounted(() => {
    reportWindowSize();
    window.addEventListener("resize", reportWindowSize);
    window.addEventListener("scroll", reportWindowSize);
  });
  return { step };
}
